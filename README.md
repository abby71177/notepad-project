# NOTEPAD PROJECT
A simple menu driven command line notepad project that enables user to create, update, delete, append and to revert to versions of the projects created.
<br>
<b>Actors in the project</b>
1. Admin -manages userLog and also has access to notepad application
2. User -Has access to all the notepad application.
<br>
<b>Features of the project</b>
<br>
<b> User Features</b>
<ul>
<li> User SignIn</li>
<li> User Login</li>
<li> Create Project</li>
<li> Append to existing Project</li>
<li> Update an existing project</li>
<li> Remove contents from an existing project</li>
<li> Display contents of a project</li>
<li> Display details of an existing project</li>
<li> My Dashboard(to view all projects and versions created so far) </li>

<b> Admin Features</b>
<li> Admin Log - to oversee the different users and details of their project </li>
<li> Version Log -to oversee versions of each user created</li>
<li> Same features for the user are also applicable </li>
</ol>


<br><b>Structure of the project</b>
<ul>
<li>include
<ul>
<li>HeaderFiles.h</li>
<li>Notepad.cpp</li>
<li>UserAuthentication.h</li>
<li>ProjectDetails.h</li>
<li>Version.h</li>
</li>
<li>src
<ul>
<li>addAdminData.cpp</li>
<li>admin.cpp</li>
<li>appendProject.cpp</li>
<li>createProject.cpp</li>
<li>displayContents.cpp</li>
<li>main.cpp</li>
<li>removeProjectContent.cpp</li>
<li>Notepad.cpp</li>
<li>updateProjectContent.cpp</li>
<li>updateProjectDetails.cpp</li>
<li>userAuthentication.cpp</li>
<li>versionControl.cpp</li>

</li>
<li>User-Details.txt</li>
<li>Project.txt</li>
<li>Versions.txt</li>
</ul>

