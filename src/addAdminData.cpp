#include "UserAuthentication.h"
#include"Notepad.h"
void UserAuthentication::addData() {
    userDetObj.admin = true;
    strcpy_s(userDetObj.userName, "admin");
    string encrypted = encryptDecryptPass("admin");
    strcpy_s(userDetObj.password, encrypted.c_str());

    strcpy_s(userDetObj.name, "Admin-1");
    strcpy_s(userDetObj.email, "admin@gmail.com");
    strcpy_s(userDetObj.location, "Texas");
    userDetObj.no_of_projects = 0;


    ofstream fileout;
    fileout.open("User-Details.txt", ios::app | ios::binary);
    if (!fileout)
        cout << "\nCannot open file\n";
    else
    {
        userDetObj.no_of_projects = 0;
        fileout.write((char*)&userDetObj, sizeof(UserDetails));
        _mkdir(userDetObj.userName);
        Notepad::common::createDirectory(userDetObj.userName,"null",1);
        //Notepad::common::createDirectory(userDetObj.userName, "null", 2);


        fileout.close();
    }
    cout << "<------------ Admin added successfully ------------>\n";

}
