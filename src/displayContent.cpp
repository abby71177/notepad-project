#include "projectDetails.h"
#include "Notepad.h"

void ProjectDetails::displayContents(char* directory) {
    fstream newfile;
    string  projectName;
    int lineNo = 1;

    cin.get();
    cout << "Enter the Project name :: ";
    getline(cin, projectName);
    string path("");
    path = Notepad::common::returnFilepath(directory, projectName, 1);
    optional<int>result = Notepad::common::checkFile(path);
    if (result.has_value()) {
        newfile.open(path, ios::in);

        string tp;
        while (getline(newfile, tp)) {
            cout << lineNo << " " << tp << "\n";
            lineNo++;
        }
        newfile.close();

    }
    else
        cout << "No such file exists\n";



}

void ProjectDetails::projectInfo(char* directory) {               // to display project details of a particular product
    int found = 0;
    string file;

    cin.get();
    cout << "\nEnter Project Id :: ";
    getline(cin, file);
    string project = directory;
    project += "/";
    project += file;

    char newPath[20];
    strcpy_s(newPath, project.c_str());
    ifstream filein;
    filein.open("Project.txt", ios::in);
    filein.read((char*)&projectObj, sizeof(projectObj));
    //while (filein)
    while (!filein.eof())
    {
        if (projectObj.id==file && strcmp(projectObj.userName,directory)==0)
        {
            found = 1;
            cout << "\Project Id :: " << projectObj.id;
            cout << "\nProject Name :: " << projectObj.projectName;
            cout << "\nNo of Operations done so far :: " << projectObj.no_of_operations;
            cout << "\nNo of Versions of this project  created so far ::" << projectObj.no_of_versions << "\n\n";
            break;
        }
        else
            filein.read((char*)&projectObj, sizeof(projectObj));
    }
    if (found == 0)
        cout << "\nNo such file exists\n";
    //cin.get();
    filein.close();

}

void ProjectDetails::userDashboard(char* user) {
    int found = 0;
    ifstream filein;
    filein.open("Project.txt", ios::in);
    filein.read((char*)&projectObj, sizeof(projectObj));
    cout << "\n\nProjects created by User - " << user << "\n\n";

    while (!filein.eof())

    {
        if (strcmp(projectObj.userName, user)==0)
        {
            found = 1;
            cout << "\Project Id :: " << projectObj.id;
            cout << "\nProject Name :: " << projectObj.projectName;
            //cout << "\Project Id :: " << projectObj.id;
            cout << "\nNo of Operations done so far :: " << projectObj.no_of_operations;
            //cout << "\nNo of Versions of this project  created so far ::" << projectObj.no_of_versions << "\n\n";
            cout << "\n<--- Versions created for this project - " << projectObj.projectName << " --->";
            VersionControl::displayVersions(2,  user, projectObj.projectName);
            cout << endl<<endl<<endl;           
        }
        filein.read((char*)&projectObj, sizeof(projectObj));
    }
    if (found == 0)
        cout << "\nProjects are not yet created by this user\n";
    //cin.get();
    filein.close();

}