#include "ProjectDetails.h"
#include "UserAuthentication.h"

int ProjectDetails::returnOperationCount(string projName,string user, int item) {         //to return either noOfOperations or no of versions od a project
    ifstream filein;
    filein.open("Project.txt", ios::in | ios::binary);
    filein.read((char*)&projectObj, sizeof(projectObj));
    while (filein)
    {
        if (projName == projectObj.projectName &&user== projectObj.userName){
            switch (item) {
            case 1: return projectObj.no_of_operations; break;
            case 2: return projectObj.no_of_versions; break;
            }
        }
        filein.read((char*)&projectObj, sizeof(projectObj));
    }
    filein.close();

}


void ProjectDetails::updateOperations(string name,string user, int variable) {      //variable=1 to increment operations and 2 to increment version
    //cin.get();
    //cout << name;
    ifstream filein;
    filein.open("Project.txt", ios::in | ios::binary);
    ofstream filout;
    fstream file;

    file.open("Project2.txt", ios::out);
    file.close();
    filout.open("Project2.txt", ios::app | ios::binary);

    filein.read((char*)&projectObj, sizeof(projectObj));
    while (filein)
    {
        if (user == projectObj.userName && name==projectObj.projectName)
            switch (variable) {
            case 1: projectObj.no_of_operations += 1; break;
            case 2: projectObj.no_of_versions += 1; break;

            }

        filout.write((char*)&projectObj, sizeof(projectObj));
        filein.read((char*)&projectObj, sizeof(projectObj));

    }
    filein.close();
    filout.close();
    remove("Project.txt");
    rename("Project2.txt", "Project.txt");
}




