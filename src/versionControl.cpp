#include "ProjectDetails.h"
#include"Versions.h"
#include"Notepad.h"

ProjectDetails projectOBJ;

void VersionControl::versionLog(string Id, string projName, string user) {
    versionObj.id = Id;
    versionObj.projectName = projName;
    versionObj.userName = user;
    ofstream ofs;
    ofs.open("Versions.txt", ios::app);
    ofs.write((char*)&versionObj, sizeof(versionObj));

    ofs.close();

}
void VersionControl::displayVersions(int choice,string userName,string projectName) {
    if (choice == 1) {
        char userName[20];
        int found = 0;

        string line;
        ifstream ifs;
        ifs.open("Versions.txt");
        ifs.read((char*)&versionObj, sizeof(versionObj));
        while (ifs) {
            if (versionObj.userName == userName) {
                found = 1;

                cout << "  Version Id :: " << versionObj.id;
                //cout << "\nProject Name :: " << versionObj.projectName<<"\n";
            }
            ifs.read((char*)&versionObj, sizeof(versionObj));
            cout << "\n";
        }
        if (found == 0)
            cout << "No versions are created for the requested user\n";

        ifs.close();
        cout << "\n<-------------------------------->\n\n";

    }
    else if(choice==2) {
        string line;
        ifstream ifs;
        int found = 0;
        ifs.open("Versions.txt");
        ifs.read((char*)&versionObj, sizeof(versionObj));
        while (ifs) {

            if (versionObj.projectName == projectName && versionObj.userName==userName) {
                found = 1;
                cout << "\nVersion Id :: " << versionObj.id;
                //cout << "\nProject Name :: " << versionObj.projectName<<"\n";
            }
            ifs.read((char*)&versionObj, sizeof(versionObj));
        }
        if (found == 0)
            cout << "\nNo versions are created for this project ";

        ifs.close();
    }

}


void VersionControl::checkVersion(string projName, char* directory) {                //to check if version of a project is to be created
    int operation = projectOBJ.returnOperationCount(projName, directory,1);
    if (operation % 10 == 0)
    {

        cout << "\n<------- Version of project is created!! ------->\n";
        string path, line, id;

        
        path = Notepad::common::returnFilepath(directory, projName, 4);
        path += to_string(operation / 10);
        path += ".txt";
        id += projName;
        id += "-V";
        id += to_string(operation / 10);
        cout << "Version Id :: " << id << "\n";
        ifstream fin;
        fin.open(Notepad::common::returnFilepath(directory, projName, 1));
        ofstream fout;
        fout.open(path);
        while (getline(fin, line)) {

            fout << line << "\n";
        }
        projectOBJ.updateOperations(projName, directory,2);
        versionLog(id, projName, directory);

        fout.close();
        fin.close();


    }
}

void VersionControl::revertVersion(char* user) {              //to revert to a version of project
    int versionNo;
    string file, path, path2;
    cin.get();
    cout << "\nEnter the Project name you want to revert :: ";
    getline(cin, file);
    
    path = Notepad::common::returnFilepath(user, file, 1);
    cout << path << endl;


    cout << "\nEnter the version number you want to revert the project back to :: ";
    cin >> versionNo;
    int currentVersion = projectOBJ.returnOperationCount(file,user, 2);

    if (versionNo > currentVersion)
        cout << "\nInvalid Version Number/ Version not yet created\n";
    else {
        
        path2 = Notepad::common::returnFilepath(user, file, 4);
        path2 += to_string(versionNo);
        path2 += ".txt";
        cout << path2;
        ofstream ofs;
        ofs.open(path, ofstream::out | ofstream::trunc);
        ofs.close();
        string line;
        ifstream fin;
        fin.open(path2);
        ofstream temp;
        temp.open(path);
        if (!fin || !temp)
            cout << "\nNo such file is exists\n";
        else {
            getline(fin, line);
            while (fin) {
                temp << line << "\n";
                getline(fin, line);
            }
            fin.close();
            temp.close();

            cout << "\nRevertion successfully executed\n";
            
            projectOBJ.updateOperations(file, user,1);
            checkVersion(file, user);

        }

    }
}
