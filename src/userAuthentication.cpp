#include "UserAuthentication.h"
#include "Notepad.h"
char* currentUser;

bool UserAuthentication::checkAdmin(char* user) {
    ifstream filein;
    struct UserDetails userDetObj1;
    optional<int>result = Notepad::common::checkFile("User-Details.txt");
    if (result.has_value()) {
        //cout << "!!!!!!!!!!! FILE OPENED !!!!!!!!\n";
        filein.open("User-Details.txt", ios::in | ios::binary);
        if (!filein)
            std::cout << "\nUnable to open file to read\n";
        else
        {
            filein.read((char*)&userDetObj1, sizeof(userDetObj1));
            while (filein)
            {
                if (userDetObj1.admin == 1 && _strcmpi(userDetObj1.userName, user) == 0)
                    return true;
                filein.read((char*)&userDetObj1, sizeof(userDetObj1));
            }
            filein.close();

            return false;
        }
    }
    else {
        cout << "file cannot be opened\n";
    }
}


bool UserAuthentication::checkUserName(const char* user) {
    ifstream filein;
    struct UserDetails userDetObj1;
    optional<int>result = Notepad::common::checkFile("User-Details.txt");
    if(result.has_value())
    {
    filein.open("User-Details.txt", ios::in | ios::binary);
    filein.read((char*)&userDetObj1, sizeof(userDetObj1));
    while (filein)
        {
            if (_strcmpi(userDetObj1.userName, user) == 0)
                return true;
            filein.read((char*)&userDetObj1, sizeof(userDetObj1));
        }
    filein.close();

    return false;
    }
    else{
        cout << "Unable to open file\n";
    }
}

string UserAuthentication::encryptDecryptPass(string password) {
    char key = 'A';
    string output = password;

    for (int i = 0; i < password.size(); i++)
        output[i] = password[i] ^ key;

    return output;
}

bool UserAuthentication::emailCheck(string email)                       //EMAIL VALIDATION
{
    const regex pattern("(\\w+)(\\.|_)?(\\w*)@(\\w+)(\\.(\\w+))+");
    return regex_match(email, pattern);
}

void UserAuthentication::registerUser()                        // to register a new user
{


    char uName[20], pass[20];
    //string path;

    cout << "\nEnter user name :: ";
    cin >> userDetObj.userName;

    if (checkUserName(userDetObj.userName) == 1) {
        cout << "User Already exists" << endl;
        registerUser();
    }
    else {
        cout << "\nEnter your name :: ";
        cin >> userDetObj.name;
        cout << "\nEnter your emailId :: ";
        cin >> userDetObj.email;
        if (emailCheck(userDetObj.email) == 0) {
            cout << "\nInvalid email\nEnter your emailId :: ";
            cin >> userDetObj.email;
        }
        cout << "\nEnter your location :: ";
        cin >> userDetObj.location;
        cout << "\nEnter password :: ";
        cin >> userDetObj.password;
        string encrypted = encryptDecryptPass(userDetObj.password);
        strcpy_s(userDetObj.password, encrypted.c_str());

        //cout << userDetObj.userName << " " << userDetObj.password;

        ofstream fileout2;

        fileout2.open("User-Details.txt", ios::app | ios::binary);
        if (!fileout2)
            cout << "\nCannot open file\n";

        else
        {
            userDetObj.no_of_projects = 0;
            userDetObj.admin = false;

            cout << "\n";

            fileout2.write((char*)&userDetObj, sizeof(UserDetails));
            char path[30];
            strcpy_s(path, userDetObj.userName);
            strcat_s(path, "/projects");
            _mkdir(userDetObj.userName);
            _mkdir(path);
            cout << path;

            fileout2.close();
            cout << "\n...........You are now registered.......... \n\n";


        }
    }


}




bool UserAuthentication::loginUser(char* user, char* password) {             //to login an existing user
    struct UserDetails userDetObj;
    int flag = 0;

    ifstream filein;
    filein.open("User-Details.txt", ios::in | ios::binary);
    if (!filein)
    {
        std::cout << "\nUnable to open file to read\n";
    }
    else
    {
        string decrypt = encryptDecryptPass(password);
        char pass[20];
        strcpy_s(pass, decrypt.c_str());
        //cout <<"\n"<<pass;
        filein.read((char*)&userDetObj, sizeof(userDetObj));
        while (filein)
        {
            if (_strcmpi(userDetObj.userName, user) == 0 && _strcmpi(userDetObj.password, pass) == 0) {
                currentUser = userDetObj.userName;
                flag = 1;
                return true;
            }
            filein.read((char*)&userDetObj, sizeof(UserDetails));
        }
        if (flag == 0)
            return false;
        filein.close();
    }

}



