#include "ProjectDetails.h"
#include"Notepad.h"

void ProjectDetails::extendProject(char* directory) {             //to add data to existing project
    string projectName,path;
    char newFile[20];
    ofstream fout;
    ifstream fin;
    int stop;

    cin.get();

    cout << "\nEnter the Project name to which you want to append content :: ";
    getline(cin, projectName);
    
    
    path = Notepad::common::returnFilepath(directory, projectName, 1);
    optional<int>result = Notepad::common::checkFile(path);
    if (result.has_value()) {

        fin.open(path);
        fout.open(path, ios::app);
        if (fin.is_open()) {
            cout << "Enter 1 to keep adding sentences to the file and 0 to terminate\n";
                do {
                    fout << Notepad::common::fileInput();

                    cin >> stop;
                    cin.get();
                } while (stop != 0);
            }
            cout << "\nData has been appended to file\n";
            fin.close();
            fout.close();

            updateOperations(projectName, directory, 1);
            strcpy_s(newFile, projectName.c_str());

            checkVersion(projectName, directory);
        
    }
    else
        cout << "\nNo such file exists\n";


}