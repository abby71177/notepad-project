#include "ProjectDetails.h"
#include "UserAuthentication.h"
#include "Notepad.h"

char* CurrentUser;
UserDetails userObj;


int ProjectDetails::returnProjectCount(char userName[10]) {             //to return no of projects created so far
    ifstream filein;
    filein.open("User-Details.txt", ios::in | ios::binary);
    if (!filein)
    {
        std::cout << "\nUnable to open file to read\n";
    }
    else
    {
        filein.read((char*)&userObj, sizeof(UserDetails));
        while (filein)
        {
            if (strcmp(userObj.userName, userName) == 0) {
                return userObj.no_of_projects;
            }
            //fileout2.write((char*)&userObj, sizeof(userDetails));
            filein.read((char*)&userObj, sizeof(UserDetails));
        }

        filein.close();
    }
    return 0;
}

void ProjectDetails::updateProjectCount(char userName[10]) {  //1->no_of_projects 2->operation count 3->version count
    ifstream filein;
    ofstream filout;
    fstream file;
    filein.open("User-Details.txt", ios::in | ios::binary);
    

    file.open("User-Details2.txt", ios::out);
    file.close();
    filout.open("User-Details2.txt", ios::app | ios::binary);

    filein.read((char*)&userObj, sizeof(userObj));
    while (filein)
    {
        if (_strcmpi(userObj.userName, userName) == 0)
            userObj.no_of_projects += 1;


        filout.write((char*)&userObj, sizeof(userObj));
        filein.read((char*)&userObj, sizeof(userObj));

    }
    filein.close();
    filout.close();
    remove("User-Details.txt");
    rename("User-Details2.txt", "User-Details.txt");
}



void ProjectDetails::createProject(char* directory) {           // to create a project for user
    int stop;
    ofstream filout;
    string path(""), path2;


    CurrentUser = directory;
    int projectCount = returnProjectCount(directory);
    updateProjectCount(directory);


    string project = "Project ";
    projectCount++;
    project += to_string(projectCount);

    projectObj.id = "";
    projectObj.id += directory;
    projectObj.id += to_string(projectCount);
    cout << endl<<project << " is created whose Id is " << projectObj.id << "\n";
    Notepad::common::createDirectory(directory, project, 2);
    path2 = Notepad::common::returnFilepath(directory, project, 1);
    //cout << endl << endl << path2;
    ofstream MyFile(path2);
    cin.get();
    cout << "Enter 1 to keep adding sentences to the file and 0 to terminate\n";
    do {
        MyFile << Notepad::common::fileInput();
        cin >> stop;
        cin.get();

    } while (stop != 0);
    MyFile.close();

    strcpy_s(projectObj.projectName, project.c_str());
    strcpy_s(projectObj.userName, directory);
    projectObj.no_of_operations = 0;
    projectObj.no_of_versions = 0;

    filout.open("Project.txt", ios::app | ios::binary);
    if (!filout)
        cout << "Error in opening file\n";
    else
        filout.write((char*)&projectObj, sizeof(projectObj));
    filout.close();
    cin.get();
}

