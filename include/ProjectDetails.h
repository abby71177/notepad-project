#pragma once
#pragma once
#include "HeaderFiles.h"
#include"Versions.h"

struct Project {             // to store meta data of each project created by user
    string id;              //username followed by random number.
    char userName[20];      //username
    char  projectName[20];     //path
    int no_of_operations;
    int no_of_versions;

};

class ProjectDetails :public VersionControl {
    struct Project projectObj;

public:
    void createProject(char*);       //to create a project eg: Project 1,Project 2
    void extendProject(char*);       //to append data to an existing project
    void updateContent(char*);       //to update data to an existing project(either overwrite all or a particular line)
    void removeContent(char*);       //to remove data to an existing project (either all or a particular line)
    void displayContents(char*);    //to display contents of a project
    void projectInfo(char*);        //to display details of the project (ID,NO_OF_OPERATIONS,NO_OF_VERSION)
    int returnProjectCount(char userName[10]);   //to return no_of_projects created by the user
    //functions to update project details

    void updateProjectCount(char userName[10]);    //to update the no_of_projects
    int returnOperationCount(string,string, int);  //to return either no_of_operations or no_of_versions as per parameter passed
    void updateOperations(string,string, int);     //to either update no_of_operations or no_of_versions as per parameter passed
    void userDashboard(char*);      //fnction to diaplay all the projects created by the user
    void adminDashboard(char*);

};











