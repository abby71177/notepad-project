#pragma once
#pragma once
#include "HeaderFiles.h"
struct version {
    string id;           //ben
    string userName;     //ben
    string projectName;  //eg: Project 9

};

class VersionControl {
    struct version versionObj;
public:
    void checkVersion(string,char*);       //to check if version is created after every operation
    void revertVersion(char*);            //to revert to the required version
    void versionLog(string, string, string);   // to maintain version log(to write into Versions.txt)
    void displayVersions(int,string,string="null");   //to display versions of the requested user and project
};

