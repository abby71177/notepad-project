#pragma once

#include "HeaderFiles.h"


struct UserDetails {       //meta data of each user
    char userName[20];
    char password[20];
    char name[10];
    char email[20];
    char location[20];
    int no_of_projects;
    bool admin;
    UserDetails() :no_of_projects(0), admin(false) {}

};

class UserAuthentication {
    struct UserDetails userDetObj;
public:
    void registerUser();         //to register a user 
    bool loginUser(char*, char*);   //fn to enable userlogin
    bool checkUserName(const char*);   //fn to check wheather username already exixts
    bool checkAdmin(char*);            //fn to check if admin has logged in
    bool emailCheck(string);           //to validate email id
    void adminLog();                   // to manage adminLog (to display all the users,their details)
    void versionLog(vector<string>);   //to display all the versions created by the user
    void addData();                   //to add admin record
    string encryptDecryptPass(string);    //to encrpt and decrypt password
};




